#!/bin/bash

set -e -x

chown -R conan /var/lib/conan
# timeout is 3000 secs = 50 minutes. That's a lot, but some large packages keep hitting the smaller ones
su -c "gunicorn -b 0.0.0.0:9300 -w 4 -t 3000 --access-logfile - conans.server.server_launcher:app" conan
